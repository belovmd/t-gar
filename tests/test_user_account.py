import unittest
from datetime import datetime
from src.user_account import UserAccount
from tests.stubs.calculation_history_for_multi_service_stub import CalculationHistoryForMultiServiceStub
from tests.stubs.norate_tariff_stub import NoRateTariffStub
from tests.stubs.unit_based_tariff_stub import UnitBasedTariffStub
from tests.stubs.multi_tariff_service_stub import MultiTariffServiceStub
from tests.stubs.second_service_stub import SecondServiceStub
from tests.stubs.service_stub import ServiceStub
from tests.stubs.calculation_history_service_stub import CalculationHistoryServiceStub
from tests.stubs.balance_stub import BalanceStub


class TestUserAccount(unittest.TestCase):

    UNIT_RATE = 0.8
    empty_uncalculated_fees = [{}]
    single_uncalculated_fees = [{datetime(2001, 4, 20): 200.0}]
    payment_dates = [datetime(2001, 2, 22), datetime(2001, 1, 23), datetime(2001, 4, 19)]
    double_uncalculated_fees = [{datetime(2001, 4, 20): 200.0}, {datetime(2001, 5, 22): 150.0}]
    another_double_uncalculated_fees = [{datetime(2001, 6, 25): 120.0}, {datetime(2001, 5, 25): 180.0}]

    def setup_services(self, services):
        user_account = UserAccount()
        user_account.set_payment_dates(self.payment_dates)
        user_account.set_balance(BalanceStub())
        user_account.set_services(services)
        return user_account

    def get_multi_tariff_service_stub(self, tariffs):
        return MultiTariffServiceStub(tariffs)

    def setup_calculation_history_service(self, user_account, calculationHistoryServiceStub):
        user_account.set_calculation_history_service(calculationHistoryServiceStub)

    def verify_updated_sum(self, user_account, expected_sum):
        user_account.balance.verify_updated_sum(expected_sum)
        user_account.calculation_history_service.verify_applied_sum(expected_sum)

    def test_should_not_apply_when_fees_already_calculated(self):
        user_account = self.setup_services([ServiceStub()])
        self.setup_calculation_history_service(
            user_account, CalculationHistoryServiceStub(self.empty_uncalculated_fees))

        user_account.recalculate_balance()
        self.verify_updated_sum(user_account, 0.0)

    def test_should_apply_sum_of_all_not_calculated_fees(self):
        user_account = self.setup_services([ServiceStub()])
        self.setup_calculation_history_service(user_account, 
            CalculationHistoryServiceStub(self.double_uncalculated_fees))

        user_account.recalculate_balance()
        self.verify_updated_sum(user_account, 350.0 * TestUserAccount.UNIT_RATE)

    def test_should_apply_sum_for_tariff_with_highest_rate(self):
        user_account = self.setup_services([self.get_multi_tariff_service_stub(
            [UnitBasedTariffStub(), NoRateTariffStub()])])
        self.setup_calculation_history_service(user_account, 
            CalculationHistoryServiceStub(self.double_uncalculated_fees))

        user_account.recalculate_balance()
        self.verify_updated_sum(user_account, 350.0)

    def test_should_apply_sum_for_tariff_with_additional_fee_for_each_uncalculated_fee(self):
        user_account = self.setup_services([self.get_multi_tariff_service_stub(
            [NoRateTariffStub(10.0), NoRateTariffStub()])])
        self.setup_calculation_history_service(user_account, 
            CalculationHistoryServiceStub(self.double_uncalculated_fees))

        user_account.recalculate_balance()
        self.verify_updated_sum(user_account, 350.0 + 10.0 + 10.0)

    def test_should_apply_sum_for_tariff_with_additional_fee_when_its_higher_than_other_tariff(self):
        user_account = self.setup_services([self.get_multi_tariff_service_stub(
            [NoRateTariffStub(), UnitBasedTariffStub(50.0)])])
        self.setup_calculation_history_service(user_account, 
            CalculationHistoryServiceStub(self.single_uncalculated_fees))

        user_account.recalculate_balance()
        self.verify_updated_sum(user_account, 200.0 * TestUserAccount.UNIT_RATE + 50.0)

    def test_should_apply_sum_for_tariff_with_highest_rate_when_its_higher_than_other_tariff(self):
        user_account = self.setup_services([self.get_multi_tariff_service_stub(
            [NoRateTariffStub(), UnitBasedTariffStub(10.0)])])
        self.setup_calculation_history_service(user_account, 
            CalculationHistoryServiceStub(self.single_uncalculated_fees))

        user_account.recalculate_balance()
        self.verify_updated_sum(user_account, 200.0)

    def test_should_apply_sum_of_all_not_calculated_fees_for_all_services(self):
        user_account = self.setup_services([ServiceStub(), SecondServiceStub()])
        
        calculationHistoryService = CalculationHistoryForMultiServiceStub(
            self.single_uncalculated_fees, self.another_double_uncalculated_fees)
        
        self.setup_calculation_history_service(user_account, calculationHistoryService)

        user_account.recalculate_balance()
        
        user_account.calculation_history_service.verify_applied_sum_for_service(
            (120.0 + 180.0) * TestUserAccount.UNIT_RATE, type(SecondServiceStub))
