from .unit_based_tariff_stub import UnitBasedTariffStub


class ServiceStub:
    def get_tariffs(self):
        return [UnitBasedTariffStub()]

