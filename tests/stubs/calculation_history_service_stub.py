from .history_stub import HistoryStub 


class CalculationHistoryServiceStub:
    def __init__(self, uncalculated_fees):
        self.historyStub = HistoryStub(uncalculated_fees)

    def retrieve_history(self, service):
        return self.historyStub

    def verify_applied_sum(self, expected_sum):
        self.historyStub.verify_applied_sum(expected_sum)