class UnitBasedTariffStub:
    def __init__(self, additional_fee=0):
        self.additional_fee = additional_fee

    def get_additional_fee(self):
        return self.additional_fee

    def get_type(self):
        return UnitBasedTariffTypeStub()


class UnitBasedTariffTypeStub:
    def is_unit_based(self):
        return True
