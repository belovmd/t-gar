---
focus: src/user_account.py
---
### Finish

Ok, seems like we are done! Let's re-read our code and make sure that it’s 
clean.

During the story, we learned that even complex functions with a big number of 
arguments can be refactored in a simple way, even without creating new classes. 
Unit tests and refactoring capabilities of the IDE are a great help. You always 
can *play around* with the code by trying different kinds of refactoring in 
different places.

Keep your code clean and readable.


<quiz>
  <question>What is the right way to reduce the number of arguments?</question>
  <answer>Analyze how each of the argument is used</answer>
  <answer>Play with the code using refactoring tools and unit tests</answer>
  <answer>Extract classes</answer>
  <answer correct>All of the above</answer>
</quiz>
